<?php

require($argv[1]);

try
{
    echo my_division_modulo(1, '/', 1);
    echo "\n";
}
catch (Exception $err)
    {
        echo $err->getMessage();
    }

try
{
    echo my_division_modulo(1, '/', 0);
    echo "\n";
}
catch (Exception $err)
    {
        echo $err->getMessage();
    }

try
{
    echo my_division_modulo(1, '+', 1);
    echo "\n";
}
catch (Exception $err)
    {
        echo $err->getMessage();
    }

try
{
    echo my_division_modulo(5, '/', 1);
    echo "\n";
}
catch (Exception $err)
    {
        echo $err->getMessage();
    }

