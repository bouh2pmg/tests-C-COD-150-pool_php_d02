<?php
require($argv[1]);

class Myclass {

}

class Zog {

}

$args = [
true,
76,
false,
12.5,
"Coucou !",
[1, 2, 3],
new MyClass(),
NULL
];

print_r(my_order_class_name(...$args));

$args = [
"ZOG",
new Zog(),
'z',
'o',
'g',
21,
42,
42.42,
NULL,
4242.4242,
[4, 2, 4, 2]
];

print_r(my_order_class_name(...$args));

?>