<?php
require($argv[1]);

$map = array();

my_create_continent("europe", $map);
var_dump($map);

my_create_country("france", "europe", $map);
my_create_country("espagne", "europe", $map);
var_dump($map);
my_create_city("marseille", "13000", "france", "europe", $map);
my_create_city("paris", "75000", "france", "europe", $map);
my_create_city("villejuif", "94800", "france", "europe", $map);
my_create_city("barcelone", "99999", "espagne", "europe", $map);
var_dump($map);

get_country_of_continent("europe", $map);
get_cities_of_country("france", "europe", $map);
get_city_postal_code("marseille", "france", "europe", $map);
?>