<?php
require($argv[1]);

var_dump(simplify_polynomial_expression("(1x^5-4x^2+3)(2x^2+3)"));
var_dump(simplify_polynomial_expression("(1x^3)(3x^10+2)"));
var_dump(simplify_polynomial_expression("(2x^2+4)(6x^3+3)"));
var_dump(simplify_polynomial_expression("(2x^3)(-3x^3-2)"));
var_dump(simplify_polynomial_expression("(1x^2-4)(2x^-2+1)"));
var_dump(simplify_polynomial_expression("(1x^3-3)(2x^2+1)"));
var_dump(simplify_polynomial_expression("(2x^0)(1)"));
var_dump(simplify_polynomial_expression("(-1y^10)(-1y^12)"));
var_dump(simplify_polynomial_expression("(-1p^1+3)(-1p^2-1p^2)"));
var_dump(simplify_polynomial_expression("(3x)(-7x^3+3)"));

?>