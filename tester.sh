#!/bin/bash

exercise=ex_$1
binary=$exercise.php
output="output.test"

function play_mouli
{
    if [[ $# -eq 2 ]] && [[ $2 -eq "-c" ]]
    then
	cd ./tests/$exercise && php moulinette.php $binary &> "../${exercise}_REF"
    else
	sudo -u student php ./tests/$exercise/moulinette.php ./$exercise/$binary &> output.test
	res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))

	if [ $? -ne 0 ]; then
	    echo "KO: check your traces below this line..."
	    echo "$res"
	else
	    echo "OK"
	fi
    fi
}

if [ $# -lt 1 ]
then
    echo -e "[ERREUR] $0 : pas assez d'arguments"
    echo -e "USAGE : $0 exercice [-c]"
    exit 2
fi

if [ ! -e $exercise ]
then
    echo "KO: exercise [$binary] not found"
    exit 2
fi

case "$exercise" in
    "ex_01")
	if [[ $# -eq 2 ]] && [[ $2 -eq "-c" ]]
	then
	    cd ./tests/$exercise && sh $exercise.sh ${exercise}_1.php &> "../${exercise}_REF"
	else
	    sudo -u student sh ./tests/$exercise/$exercise.sh ./$exercise/${exercise}_1.php &> output.test
	    res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))

	    if [ $? -ne 0 ]; then
		echo "KO: check your traces below this line..."
		echo "$res"
	    else
		echo "OK"
	    fi
	fi
    ;;
    "ex_03")
	if [[ $# -eq 2 ]] && [[ $2 -eq "-c" ]]
	then
	    echo "admin" > "tests/${exercise}_REF"
	else
	    sudo -u student cat ./tests/$exercise/$binary &> output.test
	    res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))
	    
	    if [ $? -ne 0 ]; then
		echo "KO: check your traces below this line..."
		echo "$res"
	    else
		echo "OK"
	    fi
	fi
    ;;
    *)
	play_mouli $@
    ;;
esac
